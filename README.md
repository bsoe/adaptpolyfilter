Adaptive Windowing Polynomial Filter
====================================

Brian Soe  
bsoe (at) stanford edu  
Stanford AI Lab

### About ###

AdaptPolyFilter provides the following benefits:

* Position, velocity, and acceleration estimates in real-time
* Requires position input updates
* Smooth velocity and acceleration from differentiating polynomial fit
* Flat frequency response (magnitude and phase) from adaptive window size
* Effective "cutoff" frequency is limited by signal noise and sampling rate.  
* Run time for each update is O(N) where N is maximum window size

The filter fits a 2nd order polynomial to the most recent K values,  
where K is determined based upon the user-defined expected signal noise.  

Includes code for c++ and Matlab. The c++ uses the Eigen library.

### c++ and Matlab Example ###

```c++
#include "AdaptPolyFilter.h"

AdaptPolyFilter<double> filter;
filter.sampling_freq_ = 1000.0;
filter.noise_stddev_ = 0.001;
filter.setMaxWindowSize(50);

for (int i=0; i<200; ++i)
{
  double x =  sin(2*M_PI*10.0*i/1000);
  filter.update(x);

  cout  << filter.position() << '\t'
        << filter.velocity() << '\t'
        << filter.acceleration() << '\n'

}
```

```Matlab
APF = AdaptPolyFilter;
APF.sampling_freq = 1000;
APF.noise_stddev = 0.001;
APF.setMaxWindowSize(50);

t = [0:0.001:0.2];
x = sin(2*pi*t);

for i = 1:length(x)
    APF.update(x(i));

    APF.position
    APF.velocity
    APF.acceleration
end
```

Contains a c++ and a Matlab example. To build the c++ example:  

```sh

cd AdaptPolyFilter/example_cpp
mkdir build && cd build
cmake .. && make
./adapt_poly_filter
```

Plot the result in Matlab   : plot_data.m  
Or directly access the output data : data.txt


### Compare to Euler, Kalman, Butterworth, Savitzky-Golay ###

Run in Matlab: filter_comparison/simulation.m


### Frequency Response ###

AdaptPolyFilter has a flatter frequency response than traditional filtering methods. It's effective "cutoff" frequency is determined by the sampling rate and noise value. As noise increases, phase delay increases, and velocity is slightly overestimated. 

Run in Matlab: filter_comparison/frequency_response.m  

Parameter           | Value | Unit
---                 | ---   | ---
sampling freq       | 1000  | Hz
discretization      | 1000  | states
signal noise        | 0     | 
expected max noise  | 0.002 |
max window size     | 50    | samples

![frequency response](https://bitbucket.org/bsoe/adaptpolyfilter/raw/master/filter_comparison/frequency_response.jpg)

### Tracking ###

Parameter             | Value | Unit
---                   | ---   | ---
sampling freq         | 1000  | Hz
discretization        | 1000  | states
signal gaussian noise | 0.005 | std dev
signal uniform noise  | 0.001 | 
expected max noise    | 0.010 |
max window size       | 50    | samples

![frequency response](https://bitbucket.org/bsoe/adaptpolyfilter/raw/master/example_matlab/tracking_1_hz.jpg)

![frequency response](https://bitbucket.org/bsoe/adaptpolyfilter/raw/master/example_matlab/tracking_10_hz.jpg)

![frequency response](https://bitbucket.org/bsoe/adaptpolyfilter/raw/master/example_matlab/tracking_50_hz.jpg)