clc; clear all; close all;

addpath(genpath('..'));

%% variables

% real signal
A       = 1;            % amplitude
F       = 10;           % frequency
N_cycle = 3;            % number of periods to simlulate

% sampled signal
Fs      = 1000;         % sampling frequency
res     = 5000;         % number of discrete states for x 
A_noise = 20/res;       % range: 0-1, normalized to A

% Kalman Filter
km_var_x = A_noise^2;   % variance in x
km_var_v = km_var_x*Fs; % variance in v
km_var_a = km_var_v*Fs; % variance in a

% Low pass butterworth filter
butter_order = 2;       % order
butter_wn    = 200;     % corner frequency

% Savitzky-Golay filter
sg_order = 2;           % polynomial order
sg_win   = 11;          % window size, must be odd

% Adaptive Windowing
aw_noise    = 1.5*A_noise; % peak std dev in noise
aw_max_win  = 50;        % max window size

% Plotting
plot_real       = true;
plot_measured   = false;
plot_kalman     = true;
plot_euler      = false;
plot_butter     = true;
plot_sv         = true;
plot_aw         = true;

%% plotting variables
figure; hold on;
cf_x = subplot(3,1,1); hold on;
cf_v = subplot(3,1,2); hold on;
cf_a = subplot(3,1,3); hold on;
x_legend = {};
v_legend = {};
a_legend = {};

%% real signal
dt_cont = 0.0001; % time step for continuous approx
phase = 3*pi/2;   % phase
offset = A;     % offset

t_sim = [0:dt_cont:N_cycle/F];
x_real = A*sin(2*pi*F*t_sim+phase)+offset;
v_real = 2*pi*F*A*cos(2*pi*F*t_sim+phase);
a_real = -(2*pi*F)^2*A*sin(2*pi*F*t_sim+phase);

% pad with zeros
t_extra = [(t_sim(end)+dt_cont):dt_cont:(t_sim(end)+0.25*N_cycle/F)];
t_sim = [t_sim t_extra];
N_zeros = length(t_extra);
x_real = [zeros(1,N_zeros) x_real];
v_real = [zeros(1,N_zeros) v_real];
a_real = [zeros(1,N_zeros) a_real];

axes(cf_x);
plot(t_sim,x_real);
title('position');
x_legend = [x_legend 'real'];

if plot_real
    axes(cf_v);
    plot(t_sim,v_real);
    title('velocity');
    v_legend = [v_legend 'real'];

    axes(cf_a);
    plot(t_sim,a_real);
    title('acceleration');
    a_legend = [a_legend 'real'];
end

%% sampled signal
dT = 1/Fs;
i_sample = [1:dT/dt_cont:length(t_sim)];
N = length(i_sample);
t_measure = t_sim(i_sample);

x_measure = x_real(i_sample);           % sample
x_measure = round(x_measure/A*res)/res; % descretize

%% noise
x_noise = A_noise*randn(1,N);           % gaussian noise
%x_noise = A_noise*A*wgn(1,N,1);        % white gaussian noise
%x_noise = A*noise*rand(1,N);           % even distribution noise
x_measure = x_measure + x_noise;

if plot_measured
    axes(cf_x)
    stairs(t_measure,x_measure);
    x_legend = [x_legend,'measured'];
end

% figure;
% hist(x_noise);
% title('gaussian noise distribution')

%% eueler velocity and acceleration
v_euler = [0 (x_measure(2:end)-x_measure(1:end-1))/dT];
a_euler = [0 (v_euler(2:end)-v_euler(1:end-1))/dT];

if plot_euler
    axes(cf_v)
    stairs(t_measure,v_euler);
    v_legend = [v_legend 'euler'];

    axes(cf_a)
    stairs(t_measure,a_euler);
    a_legend = [a_legend 'euler'];
end

%% kalman filter

% x = Ax + Bu + w
% z = Hx + v
% where w ~ N(0,Q) meaning w is gaussian noise with covariance Q
%       v ~ N(0,R) meaning v is gaussian noise with covariance R
clearvars s;
s.A = [1 dT 0.5*(dT^2); 0 1 dT; 0 0 1];
s.B = [0; 0; 0];
s.H = [1 0 0];
s.Q = [km_var_x 0 0; ...
       0 km_var_v 0; ...
       0 0 km_var_a];
s.R = [km_var_x];

% % Specify an initial state:
s.x = [0; 0; 0];
s.P = s.Q;

% increment kalman simulation
for i=1:N
    s(end).u = 0;
    s(end).z = x_measure(i);
    s(end+1)=kalmanf(s(end)); % perform a Kalman filter iteration
end

% unpack kalman object
km_state = [s(2:end).x]';
x_kalman = km_state(:,1);
v_kalman = km_state(:,2);
a_kalman = km_state(:,3);

if plot_kalman
    axes(cf_x)
    stairs(t_measure,x_kalman);
    x_legend = [x_legend,'kalman'];

    axes(cf_v)
    stairs(t_measure,v_kalman);
    v_legend = [v_legend 'kalman'];

    axes(cf_a)
    stairs(t_measure,a_kalman);
    a_legend = [a_legend 'kalman'];
end

%% low pass butterworth
[b,a] = butter(butter_order, 2*butter_wn/Fs);

x_butter = zeros(1,N);
x_butter(1:butter_order) = x_measure(1:butter_order);
for i=(1+butter_order):N
    x_butter(i) = (b*x_measure(i:-1:(i-butter_order))'...
                  -a(2:end)*x_butter(i-1:-1:(i-butter_order))')...
                  /a(1);
end

v_butter = [0 (x_butter(2:end)-x_butter(1:end-1))/dT];

v_butter2 = zeros(1,N);
%v_butter2(1:filter_order) = v_butter(1:filter_order);
for i=(1+butter_order):N
    v_butter2(i) = (b*v_butter(i:-1:(i-butter_order))'...
                  -a(2:end)*v_butter2(i-1:-1:(i-butter_order))')...
                  /a(1);
end

a_butter = [0 (v_butter2(2:end)-v_butter2(1:end-1))/dT];

a_butter2 = zeros(1,N);
%a_butter2(1:filter_order) = a_butter(1:filter_order);
for i=(1+butter_order):N
    a_butter2(i) = (b*a_butter(i:-1:(i-butter_order))'...
                  -a(2:end)*a_butter2(i-1:-1:(i-butter_order))')...
                  /a(1);
end

if plot_butter
    axes(cf_x)
    stairs(t_measure,x_butter);
    x_legend = [x_legend,'butter'];

    axes(cf_v)
    stairs(t_measure,v_butter2);
    v_legend = [v_legend 'butter'];

    axes(cf_a)
    stairs(t_measure,a_butter2);
    a_legend = [a_legend 'butter'];
end

%% Savitzky-Golay filter
half_win = (sg_win-1)/2;
sg_coeff_x = sgsdf([-half_win:half_win],sg_order,0,half_win);
sg_coeff_v = sgsdf([-half_win:half_win],sg_order,1,half_win);
sg_coeff_a = sgsdf([-half_win:half_win],sg_order,2,half_win);

x_sg = zeros(1,N);
v_sg = zeros(1,N);
a_sg = zeros(1,N);
for n = sg_win:N
  % 0th derivative (smoothing only)
  x_sg(n) = sg_coeff_x*x_measure(n - 2*half_win:n)';

  % 1st differential
  v_sg(n) = sg_coeff_v*x_measure(n - 2*half_win:n)';

  % 2nd differential
  a_sg(n) = sg_coeff_a*x_measure(n - 2*half_win:n)';
end

v_sg = v_sg/dT;         % Turn differential into derivative
a_sg = a_sg/(dT*dT);    % and into 2nd derivative

if plot_sv
    axes(cf_x)
    stairs(t_measure,x_sg);
    x_legend = [x_legend,'savitsky'];

    axes(cf_v)
    stairs(t_measure,v_sg);
    v_legend = [v_legend 'savitsky'];

    axes(cf_a)
    stairs(t_measure,a_sg);
    a_legend = [a_legend 'savitsky'];
end


%% Adaptive Windowing

x_aw = zeros(1,N);
v_aw = zeros(1,N);
a_aw = zeros(1,N);
aw_win_size = zeros(1,N);

APF = AdaptPolyFilter;
APF.sampling_freq = Fs;
APF.noise_stddev = aw_noise;
APF.setMaxWindowSize(aw_max_win);

for i = 1:length(x_measure)
    
    APF.update(x_measure(i));

    x_aw(i) = APF.position;
    v_aw(i) = APF.velocity;
    a_aw(i) = APF.acceleration;
    aw_win_size(i) = APF.window_size;
    
end


if plot_aw
    axes(cf_x)
    stairs(t_measure,x_aw);
    x_legend = [x_legend,'adaptive windowing'];

    axes(cf_v)
    stairs(t_measure,v_aw);
    v_legend = [v_legend 'adaptive windowing'];

    axes(cf_a)
    stairs(t_measure,a_aw);
    a_legend = [a_legend 'adaptive windowing'];
end



%% label plots
axes(cf_x)
title('position')
legend(x_legend,'Location','NorthEast');

axes(cf_v)
title('velocity')
legend(v_legend,'Location','NorthEast');

axes(cf_a)
title('acceleration')
legend(a_legend,'Location','NorthEast');

figure
plot(aw_win_size)
