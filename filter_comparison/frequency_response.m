clc; clear all; close all;
addpath(genpath('..'));

disp('This may take a minute for the first few iteration')
disp('which has ~60,000 simulation steps')

%% variables

% real signal
A       = 1;            % amplitude
Fmin    = 0.1;          % min frequency
Fmax    = 100;          % max frequency

% sampled signal
Fs      = 1000;         % sampling frequency
res     = 1000;         % number of discrete states for x 
A_noise = 1.0/res;       % range: 0-1, normalized to A

% Adaptive Windowing
aw_noise    = 2.0*A_noise; % peak std dev in noise
aw_max_win  = 50;        % max window size


%% frequencies to try
num_F = 50;
F_all   = 10.^[log10(Fmin):(log10(Fmax)-log10(Fmin))/num_F:log10(Fmax)];
N_cycle = 6;            % number of periods to simlulate

x_mag = zeros(1,num_F);
v_mag = zeros(1,num_F);
a_mag = zeros(1,num_F);
x_phase = zeros(1,num_F);
v_phase = zeros(1,num_F);
a_phase = zeros(1,num_F);

%% plotting variables
figure; hold on;
cf_x  = subplot(4,1,1); hold on;
cf_v  = subplot(4,1,2); hold on;
cf_a  = subplot(4,1,3); hold on;
cf_ws = subplot(4,1,4);

for iF = 1:length(F_all)

cla(cf_x); hold on; x_legend = {};
cla(cf_v); hold on; v_legend = {};
cla(cf_a); hold on; a_legend = {};
cla(cf_ws);
    
%% real signal
F = F_all(iF);
dt_cont = 0.0001; % time step for continuous approx
phase = 3*pi/2;   % phase

t_sim = [0:dt_cont:N_cycle/F];
x_real = A*sin(2*pi*F*t_sim+phase)+A;
v_real = 2*pi*F*A*cos(2*pi*F*t_sim+phase);
a_real = -(2*pi*F)^2*A*sin(2*pi*F*t_sim+phase);

% pad with zeros
t_extra = [(t_sim(end)+dt_cont):dt_cont:(t_sim(end)+0.25*N_cycle/F)];
t_sim = [t_sim t_extra];
N_zeros = length(t_extra);
x_real = [zeros(1,N_zeros) x_real];
v_real = [zeros(1,N_zeros) v_real];
a_real = [zeros(1,N_zeros) a_real];

axes(cf_x);
plot(t_sim,x_real);
x_legend = [x_legend 'real'];

axes(cf_v);
plot(t_sim,v_real);
v_legend = [v_legend 'real'];

axes(cf_a);
plot(t_sim,a_real);
a_legend = [a_legend 'real'];

%% sampled signal
dT = 1/Fs;
i_sample = [1:dT/dt_cont:length(t_sim)];
N = length(i_sample);
t_measure = t_sim(i_sample);

x_measure = x_real(i_sample);           % sample
x_measure = round(x_measure/A*res)/res; % descretize

%% noise
x_noise = A_noise*randn(1,N);           % gaussian noise
%x_noise = A_noise*A*wgn(1,N,1);        % white gaussian noise
%x_noise = A*noise*rand(1,N);           % even distribution noise
x_measure = x_measure + x_noise;

%% Adaptive Windowing

x_aw = zeros(1,N);
v_aw = zeros(1,N);
a_aw = zeros(1,N);
aw_win_size = zeros(1,N);

APF = AdaptPolyFilter;
APF.sampling_freq = Fs;
APF.noise_stddev = aw_noise;
APF.setMaxWindowSize(aw_max_win);

for i = 1:length(x_measure)

    APF.update(x_measure(i));

    x_aw(i) = APF.position;
    v_aw(i) = APF.velocity;
    a_aw(i) = APF.acceleration;
    aw_win_size(i) = APF.window_size;
    
end

axes(cf_x)
stairs(t_measure,x_aw);
x_legend = [x_legend,'adaptive windowing'];

axes(cf_v)
stairs(t_measure,v_aw);
v_legend = [v_legend 'adaptive windowing'];

axes(cf_a)
stairs(t_measure,a_aw);
a_legend = [a_legend 'adaptive windowing'];

axes(cf_ws)
stairs(t_measure, aw_win_size);

%% fit
i_start = max(ceil(N_zeros*Fs*dt_cont), aw_max_win);
T_sin = t_measure(i_start:end);
X_sin = x_aw(i_start:end);
V_sin = v_aw(i_start:end);
A_sin = a_aw(i_start:end);

% position
B0 = A;     % Vertical shift
B1 = A;     % Amplitude
B2 = 2*pi*F;% Phase (Number of peaks)
B3 = phase; % Phase shift (eyeball the Curve)
x_fit = NonLinearModel.fit(T_sin,X_sin, 'y ~ b0 + b1*sin(b2*x1 + b3)', [B0, B1, B2, B3]);
%methods(x_fit)

% velocity
B0 = A;
B1 = (2*pi*F)*A;
B2 = 2*pi*F;
B3 = phase;
v_fit = NonLinearModel.fit(T_sin,V_sin, 'y ~ b0 + b1*cos(b2*x1 + b3)', [B0, B1, B2, B3]);
%methods(v_fit)

% acceleartion
B0 = A;
B1 = (2*pi*F)^2*A;
B2 = 2*pi*F;
B3 = phase;
a_fit = NonLinearModel.fit(T_sin,A_sin, 'y ~ b0 - b1*sin(b2*x1 + b3)', [B0, B1, B2, B3]);
%methods(a_fit)

% save
B_x = table2array(x_fit.Coefficients(:,1));
B_v = table2array(v_fit.Coefficients(:,1));
B_a = table2array(a_fit.Coefficients(:,1));
x_mag(iF) = abs(B_x(2)/A);
v_mag(iF) = abs(B_v(2)/(2*pi*F)*A);
a_mag(iF) = abs(B_a(2)/((2*pi*F)^2*A));
x_phase(iF) = wrapToPi(B_x(4)-phase);
v_phase(iF) = wrapToPi(B_v(4)-phase);
a_phase(iF) = wrapToPi(B_a(4)-phase);

% Generate a plot
axes(cf_x);
plot(t_measure, [zeros(1,i_start-1) x_fit.Fitted'], '--');
x_legend = [x_legend,'sine fit'];

axes(cf_v);
plot(t_measure, [zeros(1,i_start-1) v_fit.Fitted'], '--');
v_legend = [v_legend,'sine fit'];

axes(cf_a);
plot(t_measure, [zeros(1,i_start-1) a_fit.Fitted'], '--');
a_legend = [a_legend,'sine fit'];

%% label plots
axes(cf_x)
ylabel('position')
legend(x_legend,'Location','NorthWest');
title(['Frequency: ' num2str(F)])

axes(cf_v)
ylabel('velocity')
legend(v_legend,'Location','NorthWest');

axes(cf_a)
ylabel('acceleration')
legend(a_legend,'Location','NorthWest');

axes(cf_ws)
ylabel('window size');

%% pause
pause(1);

end

%% plot frequency response
figure
xva_max = ceil(max([x_mag v_mag a_mag]));

subplot(2,1,1); 
loglog(F_all, x_mag); hold on;
loglog(F_all, v_mag);
loglog(F_all, a_mag);
loglog(xlim, [1 1],'Color', 'k','LineStyle','--');
legend('position', 'velocity', 'acceleration',...
       'Location', 'SouthWest');
title('Frequency Response'); 
ylabel('Magnitude');
ylim([0.1 2]);
grid on;

subplot(2,1,2); 
semilogx(F_all, 180/pi*x_phase); hold on;
semilogx(F_all, 180/pi*v_phase); 
semilogx(F_all, 180/pi*a_phase); 
%line([xlim;xlim;xlim]', [0 0;-90 -90;-180 -180]','Color', 'k','LineStyle','--');
ylabel('Phase(deg)');
xlabel('Frequency (Hz)');
ylim([-100 10]);
grid on;
