clc; clear all; close all;
addpath(genpath('..'));

%% variables
% input signal
amplitude           = 1;
frequency           = 10;
num_discrete_steps  = 1000;
normal_noise        = 0.005; 
uniform_noise       = amplitude/num_discrete_steps;

% simulation
sampling_frequency  = 1000;
run_time            = 4/frequency;

%% create filter
APF = AdaptPolyFilter;
APF.setMaxWindowSize(50);
APF.sampling_freq   = sampling_frequency;
APF.noise_stddev    = sqrt((2.0*normal_noise)^2 + (1.5*uniform_noise)^2);
   
%% real signal
dt     = 1/sampling_frequency;
t_sim  = [0:dt:run_time];
phase  = 3*pi/2;
x_real = amplitude*sin(2*pi*frequency*t_sim+phase)+amplitude;
v_real = 2*pi*frequency*amplitude*cos(2*pi*frequency*t_sim+phase);
a_real = -(2*pi*frequency)^2*amplitude*sin(2*pi*frequency*t_sim+phase);

% pad with zeros
N_zeros = 50;
t_sim  = [t_sim [(t_sim(end)+dt):dt:(t_sim(end)+N_zeros*dt)]];
x_real = [zeros(1,N_zeros) x_real];
v_real = [zeros(1,N_zeros) v_real];
a_real = [zeros(1,N_zeros) a_real];

%% noise
N = length(t_sim);
x_noise= normal_noise*randn(1,N) + (-uniform_noise+2*uniform_noise*rand(1,N));
x_real = x_real + x_noise;

%% discretize signal
x_measure = round(x_real*num_discrete_steps/amplitude)*amplitude/num_discrete_steps;

%% run simulation
x_aw = zeros(1,N);
v_aw = zeros(1,N);
a_aw = zeros(1,N);
aw_win_size = zeros(1,N);
for i = 1:length(x_measure)

    APF.update(x_measure(i));

    x_aw(i) = APF.position;
    v_aw(i) = APF.velocity;
    a_aw(i) = APF.acceleration;
    aw_win_size(i) = APF.window_size;
    
end

%% plot real signal
figure;

subplot(4,1,1); hold on;
plot(t_sim,x_real);
stairs(t_sim,x_aw);
xlim([t_sim(1) t_sim(end)]);
ylabel('position')
legend('real','filtered','Location','NorthWest');
title(['Frequency: ' num2str(frequency) ' Hz'])

subplot(4,1,2); hold on;
plot(t_sim,v_real);
stairs(t_sim,v_aw);
xlim([t_sim(1) t_sim(end)]);
ylabel('velocity')

subplot(4,1,3); hold on;
plot(t_sim,a_real);
stairs(t_sim,a_aw);
xlim([t_sim(1) t_sim(end)]);
ylabel('acceleration')

subplot(4,1,4);
stairs(t_sim, aw_win_size);
xlim([t_sim(1) t_sim(end)]);
ylabel('window size')
xlabel('time (s)')

