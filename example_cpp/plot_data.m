clc; clear all; close all;

data = importdata('build/data.txt');
data = data.data;

t = data(:,1);
x_real = data(:,2);
v_real = data(:,3);
a_real = data(:,4);
x_measured = data(:,5);
x_filtered = data(:,6);
v_filtered = data(:,7);
a_filtered = data(:,8);
window_size = data(:,9);

subplot(4,1,1);
plot(t,x_real); hold on;
%plot(t,x_measured,'--');
plot(t,x_filtered,'--');
legend('real','filtered');
ylabel('position');

subplot(4,1,2);
plot(t,v_real); hold on;
plot(t,v_filtered,'--');
legend('real','filtered');
ylabel('velocity');

subplot(4,1,3);
plot(t,a_real); hold on;
plot(t,a_filtered,'--');
legend('real','filtered');
ylabel('accleration');

subplot(4,1,4);
stairs(t,window_size);
ylabel('window size');

xlabel('time (s)');
