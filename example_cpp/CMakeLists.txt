CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

PROJECT(adapt_poly_filter)
SET(MY_DIR ${CMAKE_CURRENT_SOURCE_DIR})

############## CMAKE OPTIONS #####################

SET(CMAKE_VERBOSE_MAKEFILE ON)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
SET(CMAKE_BUILD_TYPE Debug) #Release

############### SOURCE FILES ######################

SET(SOURCES main.cpp
)

add_executable( ${PROJECT_NAME}
                ${SOURCES} )

############### DEPENDENDCIES ######################

# Eigen
find_package (Eigen3 3.0.0)
include_directories(${EIGEN_INCLUDE_DIRS})
