
#include "../AdaptPolyFilter.h"

#include <iostream>
#include <random>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <math.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    std::cout << "Hello World!" << std::endl;

    // input signal
    double amplitude          = 1;
    double frequency          = 5;
    double num_discrete_steps = 5000;
    double normal_noise       = 0; //amplitude/num_discrete_steps;
    double uniform_noise      = amplitude/num_discrete_steps;

    // simulation
    double sampling_frequency = 1000;
    double run_time           = 3/frequency; //seconds

    // output file
    std::ofstream myfile;
    myfile.open ("data.txt");
    myfile << "time" << '\t'
           << "x_real" << '\t'
           << "v_real" << '\t'
           << "a_real" << '\t'
           << "x_discrete" << '\t'
           << "x_filter" << '\t'
           << "v_filter" << '\t'
           << "a_filter" << '\t'
           << "window_size" << '\n';

    // normal distribution of noise
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(0.0,normal_noise);

    // create filter
    AdaptPolyFilter<double> filter;
    filter.setMaxWindowSize(50);
    filter.sampling_freq_ = sampling_frequency;
    filter.noise_stddev_  = sqrt(pow(2.0*normal_noise,2) + pow(1.5*uniform_noise,2));

    // timing
    std::chrono::time_point<std::chrono::system_clock> start, end;
    double max_time = 0;

    // run simulation
    double x_real, v_real, a_real;
    int total_time_steps = run_time*sampling_frequency + filter.maxWindowSize();
    for (int i=0; i<total_time_steps; ++i)
    {
        // anti windup
        double t   =  i/sampling_frequency;
        if (i <= filter.maxWindowSize()){
            x_real = 0;
            v_real = 0;
            a_real = 0;
        } else {
            // update filter
            double t_offset = t - filter.maxWindowSize()/sampling_frequency;
            x_real =  amplitude*sin(2*M_PI*frequency*t_offset+1.5*M_PI)+amplitude
                   +  distribution(generator)
                   + (uniform_noise*rand()) / (RAND_MAX);
            v_real =  (2*M_PI*frequency)*amplitude*cos(2*M_PI*frequency*t_offset+1.5*M_PI)+amplitude/2;
            a_real = -(2*M_PI*frequency)*(2*M_PI*frequency)*amplitude*sin(2*M_PI*frequency*t_offset+1.5*M_PI)+amplitude/2;
        }

        // discretize
        double x_discrete = round(num_discrete_steps*x_real)/num_discrete_steps;

        //update filter
        start = std::chrono::system_clock::now();
        filter.update(x_discrete);
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        max_time = std::max(max_time, elapsed_seconds.count());

        myfile << t << '\t'
               << x_real << '\t'
               << v_real << '\t'
               << a_real << '\t'
               << x_discrete << '\t'
               << filter.position() << '\t'
               << filter.velocity() << '\t'
               << filter.acceleration() << '\t'
               << filter.windowSize() << '\n';

    }

    myfile.close();

    std::cout << "max update time (us): " << 1000000*max_time << std::endl;

    return 0;
}
