%{
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Brian Soe
bsoe (at) stanford edu
Stanford AI Lab
%}

classdef AdaptPolyFilter < handle
   
    properties
        % parameters
        sampling_freq = 1000;
        noise_stddev  = 1;
        
        % output
        P            = [0 0 0];
        position     = 0;
        velocity     = 0;
        acceleration = 0;
        window_size  = 0;
        
        t_current
    end
    
    properties (Access = private)        
        % window
        window_sizes
       
        % precomputed
        t
        t2
        t3
        t4
        sum_t
        sum_t2
        sum_t3
        sum_t4
        A_inv
        
        % signal
        x
        sum_x
        sum_x2
        sum_tx
        sum_t2x
       % t_current
    end
    
    methods
        % create window sizes with exponential growth
        function setMaxWindowSize(obj,max_window_size)
            growth_factor = 0.1;
            obj.window_sizes = [];
            window_size = 3;
            i = 0;
            while(window_size < max_window_size) 
                obj.window_sizes = [obj.window_sizes window_size];
                i = i+1;
                window_size = round(((1+growth_factor)^i)/growth_factor...
                                    -1/growth_factor + 3);
            end

            % precompute t^n
            max_window_size = obj.window_sizes(end);
            obj.t  = 0:(max_window_size-1);
            obj.t2 = obj.t.*obj.t;
            obj.t3 = obj.t.*obj.t2;
            obj.t4 = obj.t.*obj.t3;

            % precompute sums
            obj.sum_t  = zeros(1,max_window_size);
            obj.sum_t2 = zeros(1,max_window_size);
            obj.sum_t3 = zeros(1,max_window_size);
            obj.sum_t4 = zeros(1,max_window_size);
            for i=2:max_window_size
                obj.sum_t(i)  = obj.sum_t(i-1)  + (i-1);
                obj.sum_t2(i) = obj.sum_t2(i-1) + obj.t2(i);
                obj.sum_t3(i) = obj.sum_t3(i-1) + obj.t3(i);
                obj.sum_t4(i) = obj.sum_t4(i-1) + obj.t4(i);
            end

            % precompute least squares matrix
            num_windows = length(obj.window_sizes); 
            obj.A_inv = zeros(3,3,num_windows);

            for i=1:num_windows
                window_size = obj.window_sizes(i);
                A = [window_size             obj.sum_t(window_size)  obj.sum_t2(window_size);...
                     obj.sum_t(window_size)  obj.sum_t2(window_size) obj.sum_t3(window_size);...
                     obj.sum_t2(window_size) obj.sum_t3(window_size) obj.sum_t4(window_size)];

                obj.A_inv(:,:,i) = inv(A);
            end

            % for each time step, calculate polynomial fit
            obj.x       = zeros(1,max_window_size);
            obj.sum_x   = zeros(1,max_window_size);
            obj.sum_x2  = zeros(1,max_window_size);
            obj.sum_tx  = zeros(1,max_window_size);
            obj.sum_t2x = zeros(1,max_window_size);     
            
            obj.t_current   = 0;
            obj.position    = 0;
            obj.velocity    = 0;
            obj.acceleration= 0;
        
        end % setMaxWindowSize()
        
        
        % update filter
        function update(obj,x_measure)
            
            max_window_size = obj.window_sizes(end);
            for j = max_window_size:-1:2
                obj.x(j) = obj.x(j-1);
            end
            obj.x(1) = x_measure;

            if (obj.t_current<3)
                obj.t_current = obj.t_current+1;
                return;
            end

            obj.sum_x(1)   = x_measure;
            obj.sum_x2(1)  = x_measure^2;
            obj.sum_tx(1)  = 0;
            obj.sum_t2x(1) = 0;

            for j = 2:max_window_size
                obj.sum_x(j)   = obj.sum_x(j-1)   + obj.x(j);
                obj.sum_x2(j)  = obj.sum_x2(j-1)  + obj.x(j)^2;
                obj.sum_tx(j)  = obj.sum_tx(j-1)  + obj.t(j)*obj.x(j);
                obj.sum_t2x(j) = obj.sum_t2x(j-1) + obj.t2(j)*obj.x(j);
            end

            % try window sizes in decreasing order
            P = zeros(1,3);
            num_windows = length(obj.window_sizes);
            for j=num_windows:-1:1
                window_size = obj.window_sizes(j);
                B     = [obj.sum_x(window_size);
                         obj.sum_tx(window_size);
                         obj.sum_t2x(window_size)];

                if (window_size > (obj.t_current+1))
                    continue;
                end

                    % compute polynomial and error
                    P = obj.A_inv(:,:,j)*B;

                    residual = P(1)^2*window_size             + 2*P(1)*P(2)*obj.sum_t(window_size)  - 2*P(1)*obj.sum_x(window_size)...
                             + P(2)^2*obj.sum_t2(window_size) + 2*P(1)*P(3)*obj.sum_t2(window_size) - 2*P(2)*obj.sum_tx(window_size)...
                             + P(3)^2*obj.sum_t4(window_size) + 2*P(2)*P(3)*obj.sum_t3(window_size) - 2*P(3)*obj.sum_t2x(window_size)...
                             + obj.sum_x2(window_size);

                    x_stdev = sqrt(residual)/sqrt(window_size);

                    if (x_stdev <= obj.noise_stddev)
                        break;
                    end

                %end

            end

            obj.P = P;
            obj.window_size = window_size;

            % anti windup
            if ((obj.t_current+1) < max_window_size)
            %if ((obj.t_current+1) < window_size)
                obj.P = zeros(1,3);
                obj.window_size = 0;
            end

            % save value
            obj.position    = obj.P(1);
            obj.velocity    = -obj.P(2)*obj.sampling_freq;
            obj.acceleration= 2*obj.P(3)*obj.sampling_freq*obj.sampling_freq;
            
            obj.t_current = obj.t_current+1;
        
        end % update()
        
        
    end % methods
    
            
        
end